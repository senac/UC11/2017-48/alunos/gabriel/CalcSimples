package br.com.senac.calculadorasimples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private double primeiroNumero;
    private double segundoNumero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final TextView txtResultado = (TextView) findViewById(R.id.txtResultado);

        final EditText txtPrimeiroNumero = (EditText) findViewById(R.id.txtPrimeiroNumero);
        final EditText txtSegundoNumero = (EditText) findViewById(R.id.txtSegundoNumero);

        final Button btnSoma = (Button) findViewById(R.id.btnSoma);
        final Button btnSubtracao = (Button) findViewById(R.id.btnSubtracao);
        final Button btnDivisao = (Button) findViewById(R.id.btnDivisao);
        final Button btnMultiplicacao = (Button) findViewById(R.id.btnMultiplicacao);


        btnSoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                atribuir(txtPrimeiroNumero, txtSegundoNumero);
                txtResultado.setText(String.valueOf(primeiroNumero + segundoNumero));

            }
        });

        btnSubtracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                atribuir(txtPrimeiroNumero, txtSegundoNumero);
                txtResultado.setText(String.valueOf(primeiroNumero - segundoNumero));

            }
        });

        btnDivisao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                atribuir(txtPrimeiroNumero, txtSegundoNumero);
                txtResultado.setText(String.valueOf(primeiroNumero / segundoNumero));
            }
        });

        btnMultiplicacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                atribuir(txtPrimeiroNumero, txtSegundoNumero);
                txtResultado.setText(String.valueOf(primeiroNumero * segundoNumero));
            }
        });
    }

    private void atribuir(EditText Numero1, EditText Numero2){
        primeiroNumero = Double.parseDouble(Numero1.getText().toString());
        segundoNumero = Double.parseDouble(Numero2.getText().toString());
    }


}
